﻿using Newtonsoft.Json;
using System;
 
using System.Net;
using System.Net.Http;
 
using System.Web.Http;
using System.Configuration;
using Wigs.Apps.AzureService.API.Helpers;

namespace Devops.Controllers
{
    //[RoutePrefix("data")]
    public class HomeController : ApiController
    {
        static readonly HttpClient HttpClient;
        static readonly Uri ServiceUri;
        static readonly DataLoaderHelper DataLoaderHelper;

        static HomeController()
        {
            var searchServiceName = ConfigurationManager.AppSettings["SearchServiceName-arjun"];
            var searchServiceApiKey = ConfigurationManager.AppSettings["SearchServiceApiKey"];
            DataLoaderHelper = new DataLoaderHelper();
            ServiceUri = new Uri("https://" + searchServiceName + ".search.windows.net");
            HttpClient = new HttpClient();
            HttpClient.DefaultRequestHeaders.Add("api-key", searchServiceApiKey);
        }

        //[System.Web.Http.HttpGet]
        //[System.Web.Http.Route("get-indexes")]
        public HttpResponseMessage GetIndexes()
        {
            try
            {
                var uri = new Uri(ServiceUri, "/indexes");
                var response = DataLoaderHelper.SendSearchRequest(HttpClient, HttpMethod.Get, uri);
                response.EnsureSuccessStatusCode();
                return response;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [System.Web.Http.HttpGet]
        //[System.Web.Http.Route("get-index/{indexName}")]
        public HttpResponseMessage GetIndex(string indexName)
        {
            if (string.IsNullOrEmpty(indexName)) return Request.CreateResponse(HttpStatusCode.BadRequest);
            try
            {
                var uri = new Uri(ServiceUri, "/indexes/" + indexName);
                var response = DataLoaderHelper.SendSearchRequest(HttpClient, HttpMethod.Get, uri);
                response.EnsureSuccessStatusCode();
                return response;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //[System.Web.Http.HttpPost]
        //[System.Web.Http.Route("create-index")]
        //[System.Web.Mvc.ValidateAntiForgeryToken]
        public HttpResponseMessage CreateIndex(dynamic json)
        {
            if (json == null) return Request.CreateResponse(HttpStatusCode.BadRequest);
            try
            {
                string jsonData = JsonConvert.SerializeObject(json);

                var uri = new Uri(ServiceUri, "/indexes");
                var response = DataLoaderHelper.SendSearchRequest(HttpClient, HttpMethod.Post, uri, jsonData);
                response.EnsureSuccessStatusCode();
                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [System.Web.Http.HttpPut]
        //[System.Web.Http.Route("upload-data/{indexName}")]
        //[System.Web.Mvc.ValidateAntiForgeryToken]
        public HttpResponseMessage UploadData(string indexName, [FromBody] dynamic json)
        {
            try
            {
                string jsonData = JsonConvert.SerializeObject(json);
                var uri = new Uri(ServiceUri, "/indexes/" + indexName + "/docs/index");
                var response = DataLoaderHelper.SendSearchRequest(HttpClient, HttpMethod.Post, uri, jsonData);
                response.EnsureSuccessStatusCode();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [System.Web.Http.HttpDelete]
        //[System.Web.Http.Route("delete-index/{indexName}")]
        //[System.Web.Mvc.ValidateAntiForgeryToken]
        public HttpResponseMessage DeleteIndex(string indexName)
        {
            try
            {
                var uri = new Uri(ServiceUri, "/indexes/" + indexName);
                var response = DataLoaderHelper.SendSearchRequest(HttpClient, HttpMethod.Delete, uri);
                response.EnsureSuccessStatusCode();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}