﻿using MVCApplication.App;
using MVCApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVCApplication.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveRegister(AccountModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    new Account().SaveResgister(model.FirstName, model.LastName, model.Email, model.Age);
                    FormsAuthentication.SetAuthCookie(model.FirstName, true);
                    //if (User.Identity.IsAuthenticated)
                    //{
                    //    string Name = HttpContext.User.Identity.Name;
                    //}
                }
            }
            catch { }
             return View("Register");
            //return RedirectToAction("Index", "Home");
           // return new JsonResult { Data = true };
        }
        [Authorize(Users = "arjun")]
        public ActionResult Search()
        {
            string Name = HttpContext.User.Identity.Name;
            List<AccountModel> _lstaccount = new List<AccountModel>();
            for(int i =0;i<9;i++)
            {
                AccountModel _accountmodel = new AccountModel();
                _accountmodel.Age = 1;
                _accountmodel.Email = "ar@gmail.com";
                _accountmodel.FirstName = "arjun";
                _accountmodel.LastName = "Jadhav";
                _lstaccount.Add(_accountmodel);
            }
            return View();
        }

        public ActionResult Admin()
        {
            return View();
        }
    }
}