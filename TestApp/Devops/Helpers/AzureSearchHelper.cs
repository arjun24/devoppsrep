﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;

namespace Wigs.Apps.AzureService.API.Helpers
{
    public class AzureSearchHelper : IDisposable
    {
        static ISearchIndexClient _indexClient;
        string scoringProfile;
        string scoringParameter;
        string suggesterName;
        public AzureSearchHelper(string indexname)
        {
            var searchServiceName = ConfigurationManager.AppSettings["SearchServiceName"];
            var apiKey = ConfigurationManager.AppSettings["SearchServiceApiKey"];

            try
            {
                scoringProfile = ConfigurationManager.AppSettings["ScoringProfile"];
                scoringParameter = ConfigurationManager.AppSettings["ScoringParameter"];
                suggesterName = ConfigurationManager.AppSettings["SuggesterName"];
            }
            catch
            { }

            searchServiceName = string.IsNullOrEmpty(searchServiceName) ? "" : searchServiceName;
            apiKey = string.IsNullOrEmpty(apiKey) ? "" : apiKey;
            scoringProfile = string.IsNullOrEmpty(scoringProfile) ? "" : scoringProfile;

            // Create an HTTP reference to the catalog index
            var searchClient = new SearchServiceClient(searchServiceName, new SearchCredentials(apiKey));
            _indexClient = searchClient.Indexes.GetClient(indexname);
        }

        public DocumentSearchResult Search(string searchText, List<string> businessTitleFacet, List<string> postingTypeFacet, List<string> salaryRangeFacet, List<string> locationFacet, string sortType, string searchType, int currentPage, int totalRecords = 6)
        {
            int previousPage = currentPage - 1 >= 0 ? currentPage - 1 : 0;
            if (!searchText.Equals("*"))
            {
                searchText = searchText + "*";
            }

            // Execute search based on query string
            try
            {
                var sp = new SearchParameters()
                {
                    //SearchMode = SearchMode.All,
                    Top = totalRecords,
                    Skip = previousPage * totalRecords,
                    // Limit results
                    Select = new List<string>
                    {
                        "id",
                        "job_title",
                        "job_description",
                        "min_experiance",
                        "max_experiance",
                        "no_of_position",
                        "location",
                        "job_type",
                        "status",
                        "min_salary",
                        "max_salary",
                        "link"
                    },
                    // Add count
                    IncludeTotalResultCount = true,
                    // Add search highlights
                    HighlightFields = new List<string> { "job_title" },
                    HighlightPreTag = "<u>",
                    HighlightPostTag = "</u>",
                    // Add facets
                    Facets = new List<string> { "job_title", "job_type", "location", "min_salary,interval:50000" }
                };

                switch (searchType)
                {
                    case "any":
                        sp.SearchMode = SearchMode.Any;
                        break;
                    default:
                        sp.SearchMode = SearchMode.All;
                        break;
                }

                switch (sortType)
                {
                    case "featured":
                        sp.ScoringProfile = scoringProfile; // Use a scoring profile
                        sp.ScoringParameters = new List<ScoringParameter>();
                        sp.ScoringParameters.Add(new ScoringParameter(scoringParameter, new[] { "featured" }));
                        break;
                    case "salaryDesc":
                        sp.OrderBy = new List<string> { "min_salary desc" };
                        break;
                    case "salaryAsc":
                        sp.OrderBy = new List<string> { "min_salary" };
                        break;
                        //case "mostRecent":
                        //    sp.OrderBy = new List<string> { "job_type desc" };
                        //    break;
                }

                // Add filtering
                string filter = null;
                if (businessTitleFacet != null && businessTitleFacet.Count > 0)
                {
                    filter = "job_title eq '" + businessTitleFacet[0] + "'";
                    for (var i = 1; i < businessTitleFacet.Count; i++)
                    {
                        filter += " or job_title eq '" + businessTitleFacet[i] + "'";
                    }
                }

                if (postingTypeFacet != null && postingTypeFacet.Count > 0)
                {
                    if (filter != null)
                        filter += " and ";
                    filter += "job_type eq '" + postingTypeFacet[0] + "'";
                    for (var i = 1; i < postingTypeFacet.Count; i++)
                    {
                        filter += " or job_type eq '" + postingTypeFacet[i] + "'";
                    }
                }

                //locationFacet
                if (locationFacet != null && locationFacet.Count > 0)
                {
                    if (filter != null)
                        filter += " and ";
                    filter += "location eq '" + locationFacet[0] + "'";
                    for (var i = 1; i < locationFacet.Count; i++)
                    {
                        filter += " or location eq '" + locationFacet[i] + "'";
                    }
                }

                if (salaryRangeFacet != null && salaryRangeFacet.Count > 0)
                {
                    if (filter != null)
                        filter += " and ";
                    filter += "min_salary eq " + salaryRangeFacet[0];
                    for (var i = 1; i < salaryRangeFacet.Count; i++)
                    {
                        filter += " or min_salary eq " + salaryRangeFacet[i];
                    }
                }

                sp.Filter = filter;

                return _indexClient.Documents.Search(searchText, sp);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error querying index: {ex.Message}\r\n");
            }
            return null;
        }

        public DocumentSuggestResult Suggest(string searchText, bool fuzzy)
        {
            // Execute search based on query string 
            try
            {
                SuggestParameters sp = new SuggestParameters()
                {
                    UseFuzzyMatching = fuzzy,
                    Top = 8
                };

                return _indexClient.Documents.Suggest(searchText, suggesterName, sp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error querying index: {0}\r\n", ex.Message);
            }
            return null;
        }

        public Document LookUp(string id)
        {
            // Execute geo search based on query string
            try
            {
                return _indexClient.Documents.Get(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error querying index: {0}\r\n", ex.Message);
            }
            return null;
        }

        public void Dispose()
        {

        }
    }
}