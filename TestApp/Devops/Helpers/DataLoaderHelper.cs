﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Configuration;
using System.Net.Http;
using System.Text;

namespace Wigs.Apps.AzureService.API.Helpers
{
    public class DataLoaderHelper
    {
        static string ApiVersionString;

        static readonly JsonSerializerSettings JsonSettings;

        static DataLoaderHelper()
        {
            var apiVersion = ConfigurationManager.AppSettings["ApiVersion"];
            ApiVersionString = "api-version=" + (string.IsNullOrEmpty(apiVersion) ? "2016-09-01" : apiVersion);

            JsonSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented, // for readability, change to None for compactness
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };

            JsonSettings.Converters.Add(new StringEnumConverter());
        }

        public string SerializeJson(object value)
        {
            return JsonConvert.SerializeObject(value, JsonSettings);
        }

        public T DeserializeJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, JsonSettings);
        }

        public HttpResponseMessage SendSearchRequest(HttpClient client, HttpMethod method, Uri uri, string json = null)
        {
            var builder = new UriBuilder(uri);
            var separator = string.IsNullOrWhiteSpace(builder.Query) ? string.Empty : "&";
            builder.Query = builder.Query.TrimStart('?') + separator + ApiVersionString;

            var request = new HttpRequestMessage(method, builder.Uri);

            if (json != null)
            {
                request.Content = new StringContent(json, Encoding.UTF8, "application/json");
            }
            var res = client.SendAsync(request).Result;
            return res;
        }
        public static void EnsureSuccessfulSearchResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode) return;
            var error = response.Content?.ReadAsStringAsync().Result;
            throw new Exception("Search request failed: " + error);
        }
    }
}