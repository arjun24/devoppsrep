﻿app.controller('schemauploadfile', function ($scope, $window, $http) {
    $scope.uploadschemamodalShown = false;
    $scope.showschemaContent = function ($fileContent) {
        $scope.schemaContent = $fileContent;
    };
    $scope.ReadSchemaFile = function () {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/api/home/create-index",
            "method": "POST",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache",
                "Access-Control-Allow-Origin": "*"
            },
            "data": $scope.schemaContent
        }
         $.ajax(settings).done(function (response) {
            $scope.AlertuploadschemaStatushead = "Success";
            $scope.Alertuploadschemastatusbody = "Schema uploaded successfully."
            $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.$apply();
        }).error(function () {
             $scope.AlertuploadschemaStatushead = "Failed";
            $scope.Alertuploadschemastatusbody = "Something went wrong.Please try later."
            $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.$apply();
        });;
    }
    $scope.toggleuploadschemaModal = function () {
         $scope.OpenFade();
        $scope.OpenModal();
        $scope.Alertuploadschemahead = "Save data";
        $scope.Alertuploadschemabody = "Are you sure,you want to upload file..?"

    };
    $scope.ModeluploadschemaYes = function () {
         $scope.hideModal();
        $scope.ReadSchemaFile();
    }
    $scope.OpenFade = function () {
        $scope.fade = {
            'display': 'block',
            'z-index': 1040
        }
    }
    $scope.OpenModal = function () {
        $scope.modal = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0
        };
    }
    $scope.OpenModalStatus = function () {
        $scope.modalstatus = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0,
            'z-index': '1060'
        };
    }
    $scope.hideModalStatus = function () {
        $scope.modalstatus = {
            'display': 'none'
        };
        $scope.fade = {
            'display': 'none',
        }
    }
    $scope.hideModal = function () {
        $scope.modal = {
            'display': 'none'
        };
        $scope.fade = {
            'display': 'none'
        };
    }
});
app.controller('datauploadfile', function ($scope, $window, $http) {

    $scope.schemanames = [];
    $scope.schemanames.push('select schema');
    $scope.schemaname = "select schema";

    $scope.showdataContent = function ($fileContent) {
        $scope.datacontent = $fileContent;
    };
    $scope.ReaddataFile = function () {
        if ($scope.schemaname == 'select schema') { $scope.schemaerrormsg = 'Please select schema name';  /*alert('Please select schema name');*/ return false };
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/api/home/upload-data/" + $scope.schemaname,
            "method": "PUT",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache",
                "Access-Control-Allow-Origin": "*"
            },
            "data": $scope.datacontent
        }
         $.ajax(settings).done(function (response) {
             $scope.AlertuploaddataStatushead = "Success";
            $scope.Alertuploaddatastatusbody = "Data uploaded successfully."
            $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.$apply();

        }).error(function () {
             $scope.AlertuploaddataStatushead = "Failed";
            $scope.Alertuploaddatastatusbody = "Something went wrong.Please try later."
            $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.$apply();
        });
     }
    $scope.Getjobschemas = function () {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/api/home/GetIndexes",
            "method": "GET",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache",
                "Access-Control-Allow-Origin": "*"
            },
            "data": ''
        }
        $.ajax(settings).done(function (response) {
            $scope.schemanames = [];
            angular.forEach(response.value, function (value) {
                $scope.schemanames.push(value.name);
            });
            $scope.schemaname = $scope.schemanames[0];
            $scope.$apply();
        });
    }

    $scope.uploaddatamodalShown = false;
    $scope.toggleuploaddataModal = function () {
        if ($scope.schemaname == 'select schema') { $scope.schemaerrormsg = 'Please select schema name';  /*alert('Please select schema name');*/ return false };
         $scope.OpenFade();
        $scope.OpenModal();
        $scope.Alertuploaddatahead = "Save data";
        $scope.Alertuploaddatabody = "Are you sure,you want to upload file..?"
    };
    $scope.ModeluploaddataYes = function () {
        $scope.hideModal();
        $scope.ReaddataFile();
    }
   
    $scope.OpenFade = function () {
        $scope.fade = {
            'display': 'block',
            'z-index': 1040
        }
    }
    $scope.OpenModal = function () {
        $scope.modal = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0
        };
    }
    $scope.OpenModalStatus = function () {
        $scope.modalstatus = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0,
            'z-index': '1060'
        };
    }
    $scope.hideModalStatus = function () {
        $scope.modalstatus = {
            'display': 'none'
        };
        $scope.fade = {
            'display': 'none',
        }
    }
    $scope.hideModal = function () {
        $scope.modal = {
            'display': 'none'
        };
        $scope.fade = {
            'display': 'none'
        };
    }
});
app.controller('schemacontroller', function ($scope, $window, $http, $document) {
    $scope.rows = [];
    $scope.datas = [];
    $scope.suggesterfields = [];
     var delfieldindex = -1;
    $scope.jsonarray = [];
    $scope.Analyzeranddiv = true;
    $scope.toggleModal = function (index, btntype, submitedrow) {
        if (btntype == 'removefield') {
              $scope.OpenFade(index);
             $scope.OpenModal();
             $scope.Alertschemahead = "Delete Field";
            $scope.Alertschemabody = "Are you sure,you want to delete fields..?"
            delfieldindex = index;
        }
        else {
            if ($scope.frmschema.$invalid) return false;
            $scope.suggesterfields = [];
            if ($scope.schema.suggester) {
                $scope.suggesterfields.push($scope.schema.fieldname);
            }

            angular.forEach(submitedrow, function (value, key) {
                 
                if (value.suggestervalue) {
                    $scope.suggesterfields.push(value.fieldnamevalue);
                }
            }); 

            if ($scope.suggesterfields.length > 0) {
                if ($scope.schema.suggesterName === undefined) {
                      $scope.suggestererrormsg = 'please enter suggeter name';
                      $scope.suggesterflag = true;
                      $window.scrollTo(0, 0);
                     return 0;
                }
            }
            $scope.OpenFade();
            $scope.OpenModal();
            $scope.schmamodalShown = !$scope.schmamodalShown;

            $scope.Alertschemahead = "Save data";
            $scope.Alertschemabody = "Are you sure,you want to submit data..?"
            $scope.rows = submitedrow;
        }
    };
    $scope.Add = function () {
        $scope.rows.push({
            Fieldname: "Field name",
            Fieldnamevalue: "",
            lblFieldName: "Field name :",
            lblFieldType: "Field type :",
            EdmString: "Edm.String",
            EdmColString: "Collection(Edm.String)",
            EdmBoolean: "Edm.Boolean",
            EdmInt32: "Edm.Int32",
            EdmInt64: "Edm.Int64",
            EdmDouble: "Edm.Double",
            EdmDateTimeOffset: "Edm.DateTimeOffset",
            EdmGeographyPoint: "Edm.GeographyPoint",
            lblSearchable: "Searchable :",
            Searchabletrue: "True",
            Searchablefalse: "False",
            lblFilterable: "Filterable :",
            Filterabletrue: "True",
            Filterablefalse: "False",
            lblSortable: "Sortable :",
            Sortabletrue: "True",
            Sortablefalse: "False",
            lblFacetable: "Facetable :",
            Facetabletrue: "True",
            Facetablefalse: "False",
            lblKey: "Key :",
            keytrue: "True",
            keyfalse: "False",
            lblIndexanalyzer: "Indexanalyzer :",
            lblSearchanalyzer: "Searchanalyzer :",
            lbanalyzer: "Analyzer :",
            lblsuggester: "Suggester :",
            Sortflag: true,
            Filterflag: true,
            Sortflag: true,
            Facetflag: true,
            keyflag: true,
            DynamicDivAnalyzer: "NDynamicDivIndexAnalyzer",
            DynamicDivIndexAnalyzer: "NDynamicDivAnalyzer",
            dynaAnalyzeranddiv: true
        });


    }
    $scope.saveschema = function (rows) {

        $scope.datas = [];
        $scope.suggester = [];
        $scope.analazerdata = [];
        $scope.analazertokenfilterdata = [];

        if ($scope.frmschema.$invalid) return false;
        var Searchanalyzername = null;
        var indexanalyzername = null;
        if ($scope.schema.analazer == "SearchanalyzerandIndexanalyzer") {
            Searchanalyzername = $scope.schema.Searchanalyzername;
            if ($scope.schema.Searchanalyzername == null && $scope.schema.indexanalyzername != null) {
                Searchanalyzername = 'standard';
            }
            indexanalyzername = $scope.schema.indexanalyzername;
            if ($scope.schema.indexanalyzername == null && $scope.schema.Searchanalyzername != null) {
                indexanalyzername = 'standard';
            }
        }
        if ($scope.schema.Analyzerlowercase == false && $scope.schema.Analyzerasciifolding == false && $scope.schema.Analyzerphonetic == false) {
            $scope.schema.analyzername = null;
        }
        if ($scope.schema.SearchAnalyzerlowercase == false && $scope.schema.SearchAnalyzerasciifolding == false && $scope.schema.SearchAnalyzerphonetic == false) {
            Searchanalyzername = null;
        }
        if ($scope.schema.indexlowercase == false && $scope.schema.indexasciifolding == false && $scope.schema.indexphonetic == false) {
            indexanalyzername = null;
        }

        $scope.datas.push({
            name: $scope.schema.fieldname,
            type: $scope.schema.fieldtype,
            searchable: $scope.schema.searchable,
            retrievable: $scope.schema.retrievable,
            filterable: $scope.schema.filterable,
            sortable: $scope.schema.sortable,
            facetable: $scope.schema.facetable,
            key: $scope.schema.key,
            analyzer: $scope.schema.analyzername,
            searchAnalyzer: Searchanalyzername,
            indexAnalyzer: indexanalyzername
        });

        /* Analyzer Array*/
        if ($scope.schema.analyzername != null) {
            if ($scope.schema.Analyzerlowercase && $scope.schema.analyzername != null) {
                $scope.analazertokenfilterdata.push("lowercase");
            }
            if ($scope.schema.Analyzerasciifolding && $scope.schema.analyzername != null) {
                $scope.analazertokenfilterdata.push("asciifolding");
            }
            if ($scope.schema.Analyzerphonetic && $scope.schema.analyzername != null) {
                $scope.analazertokenfilterdata.push("phonetic");
            }
            if ($scope.analazertokenfilterdata.length > 0) {
                $scope.analazerdata.push({
                    "name": $scope.schema.analyzername,
                    "@odata.type": "#Microsoft.Azure.Search.CustomAnalyzer",
                    "tokenizer": "standard_v2",
                    "tokenFilters": $scope.analazertokenfilterdata
                });
            }
        }
        /*Search Analyzer Array*/
        if ($scope.schema.Searchanalyzername != null) {
            $scope.analazertokenfilterdata = [];
            if ($scope.schema.SearchAnalyzerlowercase && $scope.schema.Searchanalyzername != null) {
                $scope.analazertokenfilterdata.push("lowercase");
            }
            if ($scope.schema.SearchAnalyzerasciifolding && $scope.schema.Searchanalyzername != null) {
                $scope.analazertokenfilterdata.push("asciifolding");
            }
            if ($scope.schema.SearchAnalyzerphonetic && $scope.schema.Searchanalyzername != null) {
                $scope.analazertokenfilterdata.push("phonetic");
            }
            if ($scope.analazertokenfilterdata.length > 0) {
                $scope.analazerdata.push({
                    "name": $scope.schema.Searchanalyzername,
                    "@odata.type": "#Microsoft.Azure.Search.CustomAnalyzer",
                    "tokenizer": "standard_v2",
                    "tokenFilters": $scope.analazertokenfilterdata
                });
            }
        }
        /* Index Analyzer Array*/
        if ($scope.schema.indexanalyzername != null) {
            $scope.analazertokenfilterdata = [];
            if ($scope.schema.indexlowercase && $scope.schema.indexanalyzername != null) {
                $scope.analazertokenfilterdata.push("lowercase");
            }
            if ($scope.schema.indexasciifolding && $scope.schema.indexanalyzername != null) {
                $scope.analazertokenfilterdata.push("asciifolding");
            }
            if ($scope.schema.indexphonetic && $scope.schema.indexanalyzername != null) {
                $scope.analazertokenfilterdata.push("phonetic");
            }
            if ($scope.analazertokenfilterdata.length > 0) {
                $scope.analazerdata.push({
                    "name": $scope.schema.indexanalyzername,
                    "@odata.type": "#Microsoft.Azure.Search.CustomAnalyzer",
                    "tokenizer": "standard_v2",
                    "tokenFilters": $scope.analazertokenfilterdata
                });
            }
        }
 
        angular.forEach(rows, function (value, key) {
            var DynaSearchanalyzername = null;
            var Dynindexanalyzername = null;
            $scope.analazertokenfilterdata = [];

            if (value.analazer == "SearchanalyzerandIndexanalyzer") {
                DynaSearchanalyzername = value.Searchanalyzername;
                if (value.Searchanalyzername == null && value.indexanalyzername != null) {
                    DynaSearchanalyzername = 'standard';
                }

                if (DynaSearchanalyzername != 'standard') {
                    /* dynamic search analyzer array*/
                    if (value.SearchAnalyzerlowercase && DynaSearchanalyzername != null) {
                        $scope.analazertokenfilterdata.push("lowercase");
                    }
                    if (value.SearchAnalyzerasciifolding && DynaSearchanalyzername != null) {
                        $scope.analazertokenfilterdata.push("asciifolding");
                    }
                    if (value.SearchAnalyzerphonetic && DynaSearchanalyzername != null) {
                        $scope.analazertokenfilterdata.push("phonetic");
                    }
                    if ($scope.analazertokenfilterdata.length > 0) {
                        $scope.analazerdata.push({
                            "name": DynaSearchanalyzername,
                            "@odata.type": "#Microsoft.Azure.Search.CustomAnalyzer",
                            "tokenizer": "standard_v2",
                            "tokenFilters": $scope.analazertokenfilterdata
                        });
                    }
                }
                Dynindexanalyzername = value.indexanalyzername;
                if (value.indexanalyzername == null & value.Searchanalyzername != null) {
                    Dynindexanalyzername = 'standard';
                }
                if (Dynindexanalyzername != 'standard') {
                    $scope.analazertokenfilterdata = [];
                    /* Index analyzer checkbox value*/
                    if (value.indexlowercase && Dynindexanalyzername != null) {
                        $scope.analazertokenfilterdata.push("lowercase");
                    }
                    if (value.indexasciifolding && Dynindexanalyzername != null) {
                        $scope.analazertokenfilterdata.push("asciifolding");
                    }
                    if (value.indexphonetic && Dynindexanalyzername != null) {
                        $scope.analazertokenfilterdata.push("phonetic");
                    }
                    if ($scope.analazertokenfilterdata.length > 0) {
                        $scope.analazerdata.push({
                            "name": Dynindexanalyzername,
                            "@odata.type": "#Microsoft.Azure.Search.CustomAnalyzer",
                            "tokenizer": "standard_v2",
                            "tokenFilters": $scope.analazertokenfilterdata
                        });
                    }
                }
             }
            /* dynamic analyzer*/

            if (value.analyzername != null) {
                if (value.Analyzerlowercase && value.analyzername != null) {
                    $scope.analazertokenfilterdata.push("lowercase");
                }
                if (value.Analyzerasciifolding && value.analyzername != null) {
                    $scope.analazertokenfilterdata.push("asciifolding");
                }
                if (value.Analyzerphonetic && value.analyzername != null) {
                    $scope.analazertokenfilterdata.push("phonetic");
                }
                if ($scope.analazertokenfilterdata.length > 0) {
                    $scope.analazerdata.push({
                        "name": value.analyzername,
                        "@odata.type": "#Microsoft.Azure.Search.CustomAnalyzer",
                        "tokenizer": "standard_v2",
                        "tokenFilters": $scope.analazertokenfilterdata
                    });
                }
            }
            if (value.Analyzerlowercase == false && value.Analyzerasciifolding == false && value.Analyzerphonetic == false) {
                value.analyzername = null;
            }
            if (value.SearchAnalyzerlowercase == false && value.SearchAnalyzerasciifolding == false && value.SearchAnalyzerphonetic == false) {
                DynaSearchanalyzername = null;
            }
            if (value.indexlowercase == false && value.indexasciifolding == false && value.indexphonetic == false) {
                Dynindexanalyzername = null;
            }

            $scope.datas.push({
                name: value.fieldnamevalue,
                type: value.fieldtypevalue,
                searchable: value.searchablevalue,
                retrievable: value.retrievablevalue,
                filterable: value.filterablevalue,
                sortable: value.sortvalue,
                facetable: value.facetvalue,
                key: value.keyvalue,
                analyzer: value.analyzername,
                searchAnalyzer: DynaSearchanalyzername,
                indexAnalyzer: Dynindexanalyzername
            });
        });
         var suggeterjson = '';
        var analyzerjson = '';

        if ($scope.suggesterfields.length > 0) {
            $scope.suggester.push({
                "name": $scope.schema.suggesterName,
                "searchMode": "analyzingInfixMatching",
                "sourceFields": $scope.suggesterfields
            });
            suggeterjson = ',"suggesters"' + ':' + JSON.stringify($scope.suggester);
        }
        if ($scope.analazerdata.length > 0) {
            analyzerjson = ',"analyzers"' + ':' + JSON.stringify($scope.analazerdata);

        }

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/api/home/create-index",
            "method": "POST",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache",
                "Access-Control-Allow-Origin": "*"
            },
            "data": '{"@odata.context":"https://atul-search-demo.search.windows.net/$metadata#indexes/$entity","name":"' + $scope.schema.schemaname + '","corsOptions": {"allowedOrigins": [ "*" ],"maxAgeInSeconds": 300},"fields":' + JSON.stringify($scope.datas) + '' + suggeterjson + '' + analyzerjson + '}'
        }
          $.ajax(settings).done(function (response) {
            $scope.suggesterfields = [];
            $scope.AlertschemaStatushead = "Success";
            $scope.Alertschemastatusbody = "Schema created successfully."
            $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.Clear();
            $scope.$apply();

        }).error(function () {
            $scope.AlertschemaStatushead = "Failed";
            $scope.Alertschemastatusbody = "Something went wrong.Please try later."
             $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.$apply();
         });
    }
    $scope.Clear = function () {
        angular.forEach(angular.element($document[0].querySelectorAll('input[type=text], input[type=email], input[type=password]')), function (elem, index) {
            elem.value = '';
        });
        $scope.frmschema.$setPristine();
    }
     $scope.SearchnIndexdiv = false;
    $scope.Analyzerdiv = true;
    $scope.ShowAnalyzerDiv = function (value) {
        $scope.SearchnIndexdiv = value == "Y";
        $scope.Analyzerdiv = value == "N";
        if (value == "Y") {
            $scope.schema.analyzername = null;
            $scope.schema.Analyzerasciifolding = false;
            $scope.schema.Analyzerlowercase = false;
            $scope.schema.Analyzerphonetic = false;
        }
        else {
            $scope.schema.Searchanalyzername = null;
            $scope.schema.indexanalyzername = null;
            Searchanalyzername = null;
            indexanalyzername = null

            $scope.schema.SearchAnalyzerlowercase = false;
            $scope.schema.SearchAnalyzerasciifolding = false;
            $scope.schema.SearchAnalyzerphonetic = false;
            $scope.schema.indexlowercase = false;
            $scope.schema.indexasciifolding = false;
            $scope.schema.indexphonetic = false;
        }
    }
    $scope.dynShowAnalyzerDiv = function (value, row) {
        row.DynamicDivIndexAnalyzer = value == "Y";
        row.DynamicDivAnalyzer = value == "N";
        if (value == "Y") {
            row.analyzername = null;
            row.Analyzerasciifolding = false;
            row.Analyzerlowercase = false;
            row.Analyzerphonetic = false;
        }
        else {
            row.Searchanalyzername = null;
            row.indexanalyzername = null;
            Dynindexanalyzername = null;
            DynaSearchanalyzername = null;

            row.SearchAnalyzerlowercase = false;
            row.SearchAnalyzerasciifolding = false;
            row.SearchAnalyzerphonetic = false;
            row.indexlowercase = false;
            row.indexasciifolding = false;
            row.indexphonetic = false;
        }
    }
    $scope.ShowAnalyzeranddiv = function () {
        if ($scope.schema.suggester) {
            $scope.schema.analazer = 'Analyzer';
            $scope.Analyzeranddiv = false;
            $scope.schema.Searchanalyzername = null;
            $scope.schema.indexanalyzername = null;
            Searchanalyzername = null;
            indexanalyzername = null

            $scope.schema.SearchAnalyzerlowercase = false;
            $scope.schema.SearchAnalyzerasciifolding = false;
            $scope.schema.SearchAnalyzerphonetic = false;
            $scope.schema.indexlowercase = false;
            $scope.schema.indexasciifolding = false;
            $scope.schema.indexphonetic = false;
            $scope.schema.analyzername = null;
            $scope.schema.Analyzerasciifolding = false;
            $scope.schema.Analyzerlowercase = false;
            $scope.schema.Analyzerphonetic = false;
        }
        else {
            $scope.Analyzeranddiv = true;
        }
    }
    $scope.ShowDynamicAnalyzeranddiv = function (row) {
        if (row.suggestervalue) {
            row.analazer = 'Analyzer';
            row.dynaAnalyzeranddiv = false;
            row.analyzername = null;
            row.Analyzerasciifolding = false;
            row.Analyzerlowercase = false;
            row.Analyzerphonetic = false;
            row.Searchanalyzername = null;
            row.indexanalyzername = null;
            Dynindexanalyzername = null;
            DynaSearchanalyzername = null;

            row.SearchAnalyzerlowercase = false;
            row.SearchAnalyzerasciifolding = false;
            row.SearchAnalyzerphonetic = false;
            row.indexlowercase = false;
            row.indexasciifolding = false;
            row.indexphonetic = false;
        }
        else {
            row.dynaAnalyzeranddiv = true;
        }
    }
    $scope.ModelYes = function () {
        $scope.hideModal();
         if (delfieldindex != -1) {
             $scope.rows.splice(delfieldindex, 1);
             delfieldindex = -1;
        }
        else {
            $scope.saveschema($scope.rows);
        }
    }
    $scope.OpenFade = function (index) {
        $scope.fade = {
            'display': 'block',
            'z-index': 1040 + ($scope.index) + index * 10
        }
    }
    $scope.OpenModalStatus = function () {
        $scope.modalstatus = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0,
            'z-index' : '1060'
        };
    }
    $scope.OpenModal = function () {
         $scope.modal = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0
        };
    }
    $scope.hideModal = function () {
        $scope.fade = {
            'display': 'none',
        } 
        $scope.modal = {
            'display': 'none'
        };
    }
    $scope.hideModalStatus = function () {
        $scope.modalstatus = {
            'display': 'none'
        };
        $scope.fade = {
            'display': 'none',
        } 
    }
    $scope.HideNShowSuggesternAnalyzer = function () {
        if ($scope.schema.fieldtype != "Edm.String" && $scope.schema.fieldtype != "Collection(Edm.String)") {
            $scope.schema.suggester = false;
            $scope.schema.analazer = 'Analyzer';
            $scope.Analyzeranddiv = false;
            $scope.schema.Searchanalyzername = null;
            $scope.schema.indexanalyzername = null;
            Searchanalyzername = null;
            indexanalyzername = null
            $scope.Suggesterdiv = false;
            $scope.schema.SearchAnalyzerlowercase = false;
            $scope.schema.SearchAnalyzerasciifolding = false;
            $scope.schema.SearchAnalyzerphonetic = false;
            $scope.schema.indexlowercase = false;
            $scope.schema.indexasciifolding = false;
            $scope.schema.indexphonetic = false;
            $scope.schema.analyzername = null;
            $scope.schema.Analyzerasciifolding = false;
            $scope.schema.Analyzerlowercase = false;
            $scope.schema.Analyzerphonetic = false;

        }
        else {
            $scope.Suggesterdiv = true;
            $scope.Analyzerdiv = true;
            $scope.schema.analazer = 'Analyzer';
            $scope.schema.Searchanalyzername = null;
            $scope.schema.indexanalyzername = null;
            Searchanalyzername = null;
            indexanalyzername = null;
            $scope.Analyzeranddiv = true;
            $scope.SearchnIndexdiv = false;
            $scope.schema.SearchAnalyzerlowercase = false;
            $scope.schema.SearchAnalyzerasciifolding = false;
            $scope.schema.SearchAnalyzerphonetic = false;
            $scope.schema.indexlowercase = false;
            $scope.schema.indexasciifolding = false;
            $scope.schema.indexphonetic = false;
            $scope.schema.analyzername = null;
            $scope.schema.Analyzerasciifolding = false;
            $scope.schema.Analyzerlowercase = false;
            $scope.schema.Analyzerphonetic = false;
        }
    }
    $scope.HideNShowDynamicSuggesternAnalyzer = function (row) {
        if (row.fieldtypevalue != "Edm.String" && row.fieldtypevalue != "Collection(Edm.String)") {
            row.Suggesterdynadiv = false;
            row.analazer = 'Analyzer';
            row.dynaAnalyzeranddiv = false;
            row.Searchanalyzername = null;
            row.indexanalyzername = null;
            Dynindexanalyzername = null;
            DynaSearchanalyzername = null;
            row.SearchAnalyzerlowercase = false;
            row.SearchAnalyzerasciifolding = false;
            row.SearchAnalyzerphonetic = false;
            row.indexlowercase = false;
            row.indexasciifolding = false;
            row.indexphonetic = false;
            row.analyzername = null;
            row.Analyzerasciifolding = false;
            row.Analyzerlowercase = false;
            row.Analyzerphonetic = false;
            row.suggestervalue = false;
            row.DynamicDivIndexAnalyzer = false;
        }
        else {
            row.suggestervalue = false;
            row.Suggesterdynadiv = true;
            row.DynamicDivIndexAnalyzer = false;
            row.analazer = 'Analyzer';
            row.DynamicDivAnalyzer = true;
            row.Searchanalyzername = null;
            row.indexanalyzername = null;
            Dynindexanalyzername = null;
            DynaSearchanalyzername = null;
            row.dynaAnalyzeranddiv = true;
            row.SearchnIndexdiv = false;
            row.SearchAnalyzerlowercase = false;
            row.SearchAnalyzerasciifolding = false;
            row.SearchAnalyzerphonetic = false;
            row.indexlowercase = false;
            row.indexasciifolding = false;
            row.indexphonetic = false;
            row.analyzername = null;
            row.Analyzerasciifolding = false;
            row.Analyzerlowercase = false;
            row.Analyzerphonetic = false;
        }
    }
});
app.controller('singlejobcontroller', function ($scope, $window, $document) {
    $scope.singlejobdatas = [];
    $scope.singlejobs = [];
    $scope.schemanames = [];
    $scope.schemanames.push('select schema');
    $scope.schemaname = "select schema";
    $scope.newschemamodalShown = false;
    $scope.toggleNewModal = function (submitedrow) {
        if ($scope.schemaname == 'select schema') { $scope.schemaerrormsg = 'Please select schema name'; return false } else { $scope.submitted = false; };
        if ($scope.frmsinglejobposting.$invalid) return false;
        $scope.OpenFade();
        $scope.OpenModal();
        $scope.Alertnewschemahead = "Save data";
        $scope.Alertnewschemabody = "Are you sure,you want to submit data..?"
        $scope.singlejobs = submitedrow;

    };
    $scope.savesinglejob = function (singlejobs) {
        if ($scope.frmsinglejobposting.$invalid) return false;
        if ($scope.schemaname == 'select schema') { $scope.schemaerrormsg = 'Please select schema name'; return false };

        $scope.singlejobdatas = [];
        getnum();

        var object = {};
        var keywordstr = [];
        
        angular.forEach(singlejobs, function (value, key) {
            if (value.Fieldlbl == 'keywords') {
                var values = value.Fieldnamevalue.split(",");
                for (var cnt = 0; cnt < values.length; cnt++) {
                    keywordstr.push(values[cnt]);
                }
                value.Fieldnamevalue = keywordstr;
            }
            object[value.Fieldlbl.replace(/ /g, '_')] = value.Fieldnamevalue;
        });
        $scope.singlejobdatas.push(object);
        var settings = {
 
            "url": "/api/home/UploadData/" + $scope.schemaname,
            "method": "PUT",
         
            "data": '{"value":' + JSON.stringify($scope.singlejobdatas) + '}'
        }
         $.ajax(settings).done(function (response) {
            $scope.AlertsingleStatushead = "Success";
            $scope.Alertsinglestatusbody = "Save successfully."
            $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.$apply();
             $scope.Clear();
         }).error(function () {
            $scope.AlertsingleStatushead = "Failed";
            $scope.Alertsinglestatusbody = "Something went wrong.Please try later."
            $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.$apply();
        });
        
       
    };
    $scope.Clear = function () {
        angular.forEach(angular.element($document[0].querySelectorAll('input[type=text], input[type=email], input[type=password]')), function (elem, index) {
            elem.value = '';
        });
        $scope.frmsinglejobposting.$setPristine();
    }
    $scope.Getjobschemas = function () {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/api/home/GetIndexes",
            "method": "GET",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache",
                "Access-Control-Allow-Origin": "*"
            },
            "data": ''
        }
        $.ajax(settings).done(function (response) {
            $scope.schemanames = [];
            angular.forEach(response.value, function (value) {
                $scope.schemanames.push(value.name);
            });
            $scope.schemaname = $scope.schemanames[0];
            $scope.addcontrols(response.value[0].fields);
            $scope.$apply();
         });
    }
    $scope.selectchange = function () {
        
 

        $.ajax({
            url: '/api/home/GetIndex',
            type: 'GET',
            dataType: 'json',
            data: {
                indexName: $scope.schemaname
            },
            success: function (response) {
                $scope.addcontrols(response.fields);
                $scope.$apply();
            }
        });
    }
    $scope.addcontrols = function (response) {
        $scope.singlejobs = [];
        var hdnfieldname = '';
        if (response != null) {
             angular.forEach(response, function (value) {
                var fieldtype = '';
                 if (!value.key) {
                    $scope.singlejobs.push({
                        Fieldlbl: value.name.replace(/_/g, ' '),
                        Fieldname: value.name,
                        Fieldnamevalue: ""
                    });
                }
                else {
                    $scope.Fieldhdnid = value.name.replace(/_/g, ' ');
                }
            });
        }
    }

    $scope.ModelnewYes = function () {
        $scope.hideModal();
        
        $scope.savesinglejob($scope.singlejobs);
    }
    $scope.hideModal = function () {
        $scope.modal = {
            'display': 'none'
        };
        $scope.fade = {
            'display': 'none'
        };
    }
    $scope.OpenFade = function () {
        $scope.fade = {
            'display': 'block',
            'z-index': 1040  
        }
    }
    $scope.OpenModal = function () {
        $scope.modal = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0
        };
    }
    $scope.OpenModalStatus = function () {
        $scope.modalstatus = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0,
            'z-index': '1060'
        };
    }
    $scope.hideModalStatus = function () {
        $scope.modalstatus = {
            'display': 'none'
        };
        $scope.fade = {
            'display': 'none',
        }
    }
});
app.controller('deleteschmacontroller', function ($scope, $window, $http) {
    $scope.data = {};
    $scope.schemanames = [];
    $scope.schemanames.push('select schema');
    $scope.deleteschemaname = "select schema";
    $scope.Getschemas = function () {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/api/home/GetIndexes",
            "method": "GET",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache",
                "Access-Control-Allow-Origin": "*"
            },
            "data": ''
        }
        $.ajax(settings).done(function (response) {
            $scope.schemanames = [];
            angular.forEach(response.value, function (value) {
                $scope.schemanames.push(value.name);
            });
            $scope.deleteschemaname = $scope.schemanames[0];
            $scope.$apply();
        });
    }
    $scope.Deleteschemas = function () {
        if ($scope.deleteschemaname == 'select schema') { $scope.schemaerrormsg = 'Please select schema name';  /*alert('Please select schema name');*/ return false };
        var settings = {
 
            "url": "/api/home/DeleteIndex?indexName=" + $scope.deleteschemaname,
             
            "method": "DELETE",
 
        }
         $.ajax(settings).done(function (response) {
            $scope.AlertdeleteStatushead = "Success";
            $scope.Alertdeletestatusbody = "Deleted successfully."
            $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.$apply();

            $scope.Getschemas();
        }).error(function () {
            $scope.AlertdeleteStatushead = "Failed";
            $scope.Alertdeletestatusbody = "Something went wrong.Please try later."
            $scope.OpenFade();
            $scope.OpenModalStatus();
            $scope.$apply();
        });
         
     }
    $scope.deletemodalShown = false;
    $scope.toggleDeleteModal = function () {
        if ($scope.deleteschemaname == 'select schema') { $scope.schemaerrormsg = 'Please select schema name';  /*alert('Please select schema name');*/ return false };
         $scope.OpenFade();
        $scope.OpenModal();
        $scope.Alertdeletehead = "Delete Schema";
        $scope.Alertdeletebody = "Are you sure,you want to delete schema..?"
     };
    $scope.ModeldeletedataYes = function () {
        $scope.hideModal();
        $scope.Deleteschemas();
    }
    $scope.OpenFade = function () {
        $scope.fade = {
            'display': 'block',
            'z-index': 1040
        }
    }
    $scope.OpenModal = function () {
        $scope.modal = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0
        };
    }
    $scope.OpenModalStatus = function () {
        $scope.modalstatus = {
            'display': 'block',
            'position': 'fixed',
            'top': 0,
            'right': 0,
            'bottom': 0,
            'left': 0,
            'z-index': '1060'
        };
    }
    $scope.hideModalStatus = function () {
        $scope.modalstatus = {
            'display': 'none'
        };
        $scope.fade = {
            'display': 'none',
        }
    }
    $scope.hideModal = function () {
        $scope.modal = {
            'display': 'none'
        };
        $scope.fade = {
            'display': 'none'
        };
    }
});
app.directive('onlyDigits', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {
            function inputValue(val) {
                if (val) {
                    var digits = val.replace(/[^0-9]/g, '');

                    if (digits !== val) {
                        ctrl.$setViewValue(digits);
                        ctrl.$render();
                    }
                    return parseInt(digits, 10);
                }
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    };
});
app.directive('onReadFile', function ($parse) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attrs) {
            var fn = $parse(attrs.onReadFile);
            element.on('change', function (onChangeEvent) {
                var reader = new FileReader();
                reader.onload = function (onLoadEvent) {
                    scope.$apply(function () {
                        fn(scope, { $fileContent: onLoadEvent.target.result });
                    });
                };
                reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
            });
        }
    };
});
app.directive('modalDialog', function () {
    return {
        restrict: 'E',
        scope: {
            show: '='
        },
        replace: true,  
        transclude: true,  
        link: function (scope, element, attrs) {
            scope.dialogStyle = {};
            if (attrs.width)
                scope.dialogStyle.width = attrs.width;
            if (attrs.height)
                scope.dialogStyle.height = attrs.height;
            scope.hideModal = function () {
                scope.show = false;
            };
        },
        template: "<div class='ng-modal' ng-show='show'>" +
        "<div class='ng-modal-overlay' ng-click='hideModal()'></div>" +
        "<div class='ng-modal-dialog' ng-style='dialogStyle'>" +
        "<div class='ng-modal-close' ng-click='hideModal()'>X</div>" +
        "<div class='ng-modal-dialog-content' ng-transclude></div>" +
        "</div></div>"
    };
});
app.filter('customSplitString', function () {
    return function (input) {
        var arr = input.split(',');
        return arr;
    };
});
var getnum = function () {
    $('#txthdnid').val(Math.round(Math.random() * 100) + 1);
    $('#txthdnid').trigger('change');
}