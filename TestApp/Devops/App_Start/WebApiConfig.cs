﻿using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Wigs.Apps.AzureService.API
{
	public class WebApiConfig : HttpApplication
    {
		public static void Register(HttpConfiguration config)
		{
			// Enable attribute routing
			config.MapHttpAttributeRoutes();

			//Enable cors
			config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

			//Return JSON
			config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

			// Web API routes
			config.Routes.MapHttpRoute(
				"DefaultApi",
				"api/{controller}/{id}",
				new { id = RouteParameter.Optional }
			);
		}
	}
}
